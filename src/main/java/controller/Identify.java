package controller;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Identify implements Initializable {

	private EntityManagerFactory emfactory;
	private EntityManager entityManager;
	private Scene scene;
	
	@FXML
	private AnchorPane box;

	@Override
	public void initialize(java.net.URL arg0, ResourceBundle arg1) {
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Budapest"));
		emfactory = Persistence.createEntityManagerFactory("WrestlingApp");
		entityManager = emfactory.createEntityManager();
		Context.getInstance().setEntityManager(entityManager);
	}

	@FXML
	private void adminLogin() throws IOException {
		try {
			//FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/AdminLogin.fxml"));
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/SignUp.fxml"));
			AnchorPane root1 = fxmlLoader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root1));
			stage.show();
			closeButtonAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void userLogin() throws IOException {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/UserLogin.fxml"));
			AnchorPane root1 = fxmlLoader.load();

			Stage stage1 = new Stage();
			stage1.setScene(new Scene(root1));
			stage1.show();
			closeButtonAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void register() throws IOException {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/Register.fxml"));
			AnchorPane root1 = fxmlLoader.load();

			Stage stage1 = new Stage();
			stage1.setScene(new Scene(root1));
			stage1.show();
			closeButtonAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void closeButtonAction() {
		Stage stage = (Stage) box.getScene().getWindow();
		stage.close();
	}
}