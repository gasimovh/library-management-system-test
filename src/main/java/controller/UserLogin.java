package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.prefs.Preferences;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class UserLogin implements Initializable {

    private EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("WrestlingApp");
    private EntityManager entityManager = emfactory.createEntityManager();
    private AuthenticationManager auth = new AuthenticationManager(entityManager);
    
    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @Override
    public void initialize(java.net.URL arg0, ResourceBundle arg1) {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Budapest"));
        emfactory = Persistence.createEntityManagerFactory("WrestlingApp");
        entityManager = emfactory.createEntityManager();
        Context.getInstance().setEntityManager(entityManager);
        System.out.println("BBB");
    }
    
    @FXML
    public void login() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        String user = username.getText();
        String pass = password.getText();
        System.out.println("------" + user);
        System.out.println("------" + pass);
        
        if (auth.authenticateUser(user, pass.toCharArray())) {
    		try {
    			Preferences prefs = Preferences.userRoot().node(this.getClass().getName());
    			prefs.put("username", user);
    			
    			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/Menu.fxml"));
    			VBox root1 = fxmlLoader.load();
    			Stage stage = new Stage();
    			stage.setScene(new Scene(root1));
    			stage.show();
    			closeButtonAction();
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        }
    	System.out.println("aaa");
    	
    }
    
    @FXML
	private void adminSignIn() throws IOException {
		try {
			//FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/AdminLogin.fxml"));
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/SignUp.fxml"));
			AnchorPane root1 = fxmlLoader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root1));
			stage.show();
			closeButtonAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void userSignIn() throws IOException {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/UserLogin.fxml"));
			AnchorPane root1 = fxmlLoader.load();

			Stage stage1 = new Stage();
			stage1.setScene(new Scene(root1));
			stage1.show();
			closeButtonAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void signUp() throws IOException {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/Register.fxml"));
			AnchorPane root1 = fxmlLoader.load();

			Stage stage1 = new Stage();
			stage1.setScene(new Scene(root1));
			stage1.show();
			closeButtonAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    private void closeButtonAction() {
		Stage stage = (Stage) username.getScene().getWindow();
		stage.close();
	}
    
}