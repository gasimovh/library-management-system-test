package main;

import model.Apple;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DatabasePersist {

    public static void main(String[] args) {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("lms");
        EntityManager entityManager = emfactory.createEntityManager();
        entityManager.getTransaction().begin();

        Apple apple = new Apple("Amkname", "amkcat", 50, "amkmanuf");

        entityManager.persist(apple);

        entityManager.getTransaction().commit();

        entityManager.close();
        emfactory.close();
    }
}
