package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Rent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int book_code;
    private String name_book;
    private int time;


    public Rent() {}

    public Rent( int book_code,String name_book,int time) {
        this.book_code=book_code;
        this.name_book=name_book;
        this.time=time;
    }

    public int getBook_code() {
        return book_code;
    }

    public int getTime() {
        return time;
    }

    public String getName_book() {
        return name_book;
    }

}
